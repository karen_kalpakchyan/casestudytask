package com.hydrane;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class CloseQuiteBbrowser {

  public WebDriver driver;
  JavascriptExecutor jse;

  public void closeBrowser() {

    try {
      driver.quit();
      System.out.println("browser closed");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}

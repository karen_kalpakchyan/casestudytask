package com.hydrane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class WrongBirthdayData {

  private static final boolean ElemEmpty = true;
  public WebDriver driver;
  JavascriptExecutor jse;

  public void birthday() {

    try {

      driver.findElement(By.id("name")).sendKeys("Kristine Mukuchyan");
      driver.findElement(By.id("birthDate")).sendKeys("20/24/1991");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("fire");
      driver.findElement(By.id("power")).sendKeys("meteor");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("incorrect birthdate month");
      Thread.sleep(4000);

      driver.navigate().refresh();
      if (ElemEmpty) {
        driver.get("http://qa-case-study.hydrane.com");
      }
      
      driver.findElement(By.id("name")).sendKeys("Kristine Mukuchyan");
      driver.findElement(By.id("birthDate")).sendKeys("00/00/0000");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("fire");
      driver.findElement(By.id("power")).sendKeys("meteor");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("incorrect birthdateall 0s");
      Thread.sleep(4000);

      driver.navigate().refresh();
      if (ElemEmpty) {
        driver.get("http://qa-case-study.hydrane.com");
      }
      
      driver.findElement(By.id("name")).sendKeys("Kristine Mukuchyan");
      driver.findElement(By.id("birthDate")).sendKeys("80/1100/-20000");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("fire");
      driver.findElement(By.id("power")).sendKeys("meteor");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("wronf birthdate all wrongs");
      Thread.sleep(4000);

      driver.navigate().refresh();
      if (ElemEmpty) {
        driver.get("http://qa-case-study.hydrane.com");
      }
      
    } catch (InterruptedException e) {

      e.printStackTrace();
    }
  }

}
